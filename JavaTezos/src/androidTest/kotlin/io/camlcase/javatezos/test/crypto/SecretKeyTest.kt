/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.crypto

import io.camlcase.javatezos.crypto.SecretKey
import io.camlcase.javatezos.test.crypto.mnemonic.MnemonicFacadeTest.Companion.MNEMONIC
import org.junit.Assert
import org.junit.Test

internal class SecretKeyTest {
    @Test
    fun testInvalidSeedString() {
        val invalidSeedString = "abcdefghijklmnopqrstuvwxyz"
        val key = SecretKey(invalidSeedString)
        Assert.assertNull(key)
    }

    @Test
    fun testInvalidMnemonic() {
        val list = listOf("JavaTezos", "JavaTezos", "JavaTezos", "JavaTezos", "JavaTezos")
        val key = SecretKey(list)
        Assert.assertNull(key)
    }

    @Test
    fun testValidMnemonic() {
        val key = SecretKey(MNEMONIC)
        Assert.assertNotNull(key)
    }

    companion object {
        // Base58Check encoded secret key generated from the test mnemonic.
        private const val EXPECTED_SECRET_KEY =
            "edskS4pbuA7rwMjsZGmHU18aMP96VmjegxBzwMZs3DrcXHcMV7VyfQLkD5pqEE84wAMHzi8oVZF6wbgxv3FKzg7cLqzURjaXUp"
    }

}
