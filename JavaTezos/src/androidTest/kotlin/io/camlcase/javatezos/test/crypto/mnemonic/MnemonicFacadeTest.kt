/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.crypto.mnemonic

import io.camlcase.javatezos.crypto.mnemonic.MnemonicFacade
import org.junit.Assert
import org.junit.Test

/**
 * Test suite for [MnemonicFacade]
 */
internal class MnemonicFacadeTest {
    @Test
    fun testValidMnemonics() {
        val mnemonic = listOf(
            "pear",
            "peasant",
            "pelican",
            "pen",
            "pear",
            "peasant",
            "pelican",
            "pen",
            "pear",
            "peasant",
            "pelican",
            "pen",
            "pear",
            "peasant",
            "pelican ",
            "pen"
        )
        val validate = MnemonicFacade.validate(mnemonic)
        Assert.assertTrue(validate)
    }

    @Test
    fun testInvalidMnemonics() {
        val mnemonic = listOf("slacktivist", "snacktivity", "snuggie")
        val validate = MnemonicFacade.validate(mnemonic)
        Assert.assertFalse(validate)
    }

    @Test
    fun testInvalidLanguageMnemonics() {
        val mnemonic = listOf(
            "pera",
            "campesina",
            "pelican",
            "pen",
            "pera",
            "campesina",
            "pelican",
            "pen",
            "pera",
            "campesina",
            "pelican",
            "pen",
            "pera",
            "campesina",
            "pelican",
            "pen"
        )
        val validate = MnemonicFacade.validate(mnemonic)
        Assert.assertFalse(validate)
    }

    @Test
    fun testValidMixedCaseMnemonics() {
        val mnemonic = listOf("pear", "PEASANT", "PeLiCaN", "pen")
        val validate = MnemonicFacade.validate(mnemonic)
        Assert.assertTrue(validate)
    }

    @Test
    fun testValidUnsanitizedMnemonics() {
        val mnemonic = listOf("    pear", "peasant ", "pelican", "pen\t\t\n")
        val validate = MnemonicFacade.validate(mnemonic)
        Assert.assertTrue(validate)
    }

    @Test
    fun testGenerateMnemonic() {
        val mnemonic = MnemonicFacade.generateMnemonic(32)
        Assert.assertNotNull(mnemonic)
        Assert.assertTrue(mnemonic?.size == 24)
    }

    @Test
    fun testGenerateMnemonicWith0Strenght() {
        val mnemonic = MnemonicFacade.generateMnemonic(0)
        Assert.assertNull(mnemonic)
    }

    @Test
    fun testGenerateMnemonicWithBadStrength() {
        val mnemonic = MnemonicFacade.generateMnemonic(17)
        Assert.assertNull(mnemonic)
    }

    @Test
    fun testSeedStringFromMnemonicNoPassphrase() {
        val result = MnemonicFacade.seedString(MNEMONIC)
        Assert.assertEquals(EXPECTED_STRING_NO_PASSPHRASE, result)
    }

    @Test
    fun testSeedStringFromMnemonicWithPassphrase() {
        val result = MnemonicFacade.seedString(MNEMONIC, PASSPHRASE)
        Assert.assertEquals(EXPECTED_STRING_W_PASSPHRASE, result)
    }

    companion object {
        val MNEMONIC = listOf(
            "soccer",
            "click",
            "number",
            "muscle",
            "police",
            "corn",
            "couch",
            "bitter",
            "gorilla",
            "camp",
            "camera",
            "shove",
            "expire",
            "praise",
            "pill"
        )
        private const val PASSPHRASE = "TezosKitTests"

        private const val EXPECTED_STRING_NO_PASSPHRASE =
            "cce78b57ed8f4ec6767ed35f3aa41df525a03455e24bcc45a8518f63fbeda772"
        private const val EXPECTED_STRING_W_PASSPHRASE =
            "cc90fecd0a596e2cd41798612682395faa2ebfe18945a88c6f01e4bfab17c3e3"
    }
}
