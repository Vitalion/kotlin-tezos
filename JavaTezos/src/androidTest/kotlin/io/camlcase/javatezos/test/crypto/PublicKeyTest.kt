/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.crypto

import io.camlcase.javatezos.crypto.PublicKey
import io.camlcase.javatezos.crypto.SecretKey
import io.camlcase.javatezos.test.crypto.mnemonic.MnemonicFacadeTest.Companion.MNEMONIC
import org.junit.Assert
import org.junit.Test

internal class PublicKeyTest {
    @Test
    fun testBase58CheckRepresentation() {
        val publicKey = PublicKey(secretKey = secretKey)
        Assert.assertEquals(publicKey!!.base58Representation, EXPECTED_PUBLIC_KEY)
    }

    @Test
    fun testPublicKeyHash() {
        val publicKey = PublicKey(secretKey = secretKey)
        Assert.assertEquals("tz1Y3qqTg9HdrzZGbEjiCPmwuZ7fWVxpPtRw", publicKey!!.hash)
    }

    @Test
    fun testVerifyHex() {
        val hexToSign = "123456"
        val secretKey2 =
            SecretKey(listOf("soccer", "soccer", "soccer", "soccer", "soccer", "soccer", "soccer", "soccer"))
        Assert.assertNotNull(secretKey)
        Assert.assertNotNull(secretKey2)

        val publicKey1 = PublicKey(secretKey = secretKey)
        val publicKey2 = PublicKey(secretKey = secretKey2)

        val signature: ByteArray? = secretKey?.sign(hexToSign)
        Assert.assertTrue(publicKey1?.verify(signature!!, hexToSign)!!)
        Assert.assertFalse(publicKey2?.verify(signature!!, hexToSign)!!)
        Assert.assertFalse(publicKey1.verify(byteArrayOf(1, 2, 3), hexToSign))

    }

    companion object {
        private val secretKey = SecretKey(MNEMONIC)
        private const val EXPECTED_PUBLIC_KEY = "edpku9ZF6UUAEo1AL3NWy1oxHLL6AfQcGYwA5hFKrEKVHMT3Xx889A"
    }
}
