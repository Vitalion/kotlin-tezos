/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.crypto

import io.camlcase.javatezos.crypto.SigningService
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.test.util.Params.Companion.fakeMetadata
import io.camlcase.javatezos.test.util.Params.Companion.fakeOperationPayload
import io.camlcase.javatezos.test.util.RPCConstants.Companion.FORGE_RESULT
import io.camlcase.javatezos.test.util.fakeSignature
import org.junit.Assert
import org.junit.Test

class SigningServiceTest {

    @Test
    fun testOk() {
        val result = SigningService.sign(fakeOperationPayload, fakeMetadata, FORGE_RESULT, fakeSignature)
        println("$result")
        Assert.assertNotNull(result)
    }

    @Test(expected = TezosError::class)
    fun testEmptyForgeResult() {
        SigningService.sign(fakeOperationPayload, fakeMetadata, "    ", fakeSignature)
    }

    @Test(expected = TezosError::class)
    fun testNullForgeResult() {
        SigningService.sign(fakeOperationPayload, fakeMetadata, null, fakeSignature)
    }

}
