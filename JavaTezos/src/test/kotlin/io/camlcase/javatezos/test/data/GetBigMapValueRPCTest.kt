/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.data

import io.camlcase.javatezos.data.GetBigMapValueRPC
import io.camlcase.javatezos.data.RPC_PATH_BIG_MAP_SUFFIX
import io.camlcase.javatezos.data.RPC_PATH_CONTRACTS
import io.camlcase.javatezos.michelson.MichelsonComparable
import io.camlcase.javatezos.michelson.StringMichelsonParameter
import io.camlcase.javatezos.test.util.BabylonAddress.Companion.TEST_ADDRESS_TZ1_CW
import io.camlcase.javatezos.test.util.BabylonAddress.Companion.TEST_ADDRESS_TZ1_WRO
import org.junit.Assert
import org.junit.Test

class GetBigMapValueRPCTest {
    @Test
    fun testPayload() {
        val michelsonAddress = StringMichelsonParameter(TEST_ADDRESS_TZ1_CW)
        val expected =
            "{\"type\":{\"prim\":\"${MichelsonComparable.ADDRESS.name.toLowerCase()}\"},\"key\":{\"string\":\"$TEST_ADDRESS_TZ1_CW\"}}"

        val rpc = GetBigMapValueRPC(TEST_ADDRESS_TZ1_WRO, michelsonAddress, MichelsonComparable.ADDRESS)
        Assert.assertEquals(RPC_PATH_CONTRACTS + TEST_ADDRESS_TZ1_WRO + RPC_PATH_BIG_MAP_SUFFIX, rpc.endpoint)
        Assert.assertEquals(expected, rpc.payload)
        Assert.assertTrue(rpc.header.count() > 0)
    }
}
