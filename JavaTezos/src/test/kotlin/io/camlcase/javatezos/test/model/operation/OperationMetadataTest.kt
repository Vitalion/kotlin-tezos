/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.model.operation

import io.camlcase.javatezos.model.TezosProtocol
import io.camlcase.javatezos.model.operation.OperationMetadata
import org.junit.Assert
import org.junit.Test

class OperationMetadataTest {
    @Test
    fun testValidBabylonProtocol() {
        val metadata = OperationMetadata(
            "BMJ15hCiugQ17mo7g3bufaQsJqtxiWebqNYswLV3tdZB17X91WQ",
            "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS",
            "NetXdQprcVkpaWU",
            1234,
            null
        )
        Assert.assertEquals(TezosProtocol.BABYLON, metadata.tezosProtocol)
    }

    @Test
    fun testValidCarthageProtocol() {
        val metadata = OperationMetadata(
            "BMECV4c854a1TEHApLLwfhJY6iHe5g71UutXTaaos4TPWDWr1K5",
            "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb",
            "NetXjD3HPJJjmcd",
            1235,
            null
        )
        Assert.assertEquals(TezosProtocol.CARTHAGE, metadata.tezosProtocol)
    }

    @Test
    fun testInvalidProtocol() {
        val metadata = OperationMetadata(
            "BMECV4c854a1TEHApLLwfhJY6iHe5g71UutXTaaos4TPWDWr1K5",
            "PsYaddaYadda",
            "NetXjD3HPJJjmcd",
            1235,
            null
        )
        Assert.assertEquals(TezosProtocol.BABYLON, metadata.tezosProtocol)
    }
}
