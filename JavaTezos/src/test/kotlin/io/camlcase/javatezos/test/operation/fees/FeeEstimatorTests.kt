/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.operation.fees

import io.camlcase.javatezos.data.RunOperationRPC
import io.camlcase.javatezos.ext.equalsToWildcard
import io.camlcase.javatezos.model.Tez
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.model.TezosErrorType
import io.camlcase.javatezos.model.operation.OperationType
import io.camlcase.javatezos.operation.ForgingPolicy
import io.camlcase.javatezos.operation.ForgingService
import io.camlcase.javatezos.operation.SimulationService
import io.camlcase.javatezos.operation.fees.FeeEstimator
import io.camlcase.javatezos.test.network.test.*
import io.camlcase.javatezos.test.util.CurrentThreadExecutor
import io.camlcase.javatezos.test.util.Params.Companion.fakeAddress
import io.camlcase.javatezos.test.util.Params.Companion.fakeMetadata
import io.camlcase.javatezos.test.util.Params.Companion.fakeTransactionParams
import io.camlcase.javatezos.test.util.RPCConstants
import io.camlcase.javatezos.test.util.fakeSignature
import io.camlcase.javatezos.test.util.testFailure
import io.mockk.impl.annotations.SpyK
import io.mockk.spyk
import io.mockk.verify
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.junit.Assert
import org.junit.Test

/**
 * Integration test for [FeeEstimator]
 */
class FeeEstimatorTests : MockServerTest() {
    @SpyK
    lateinit var simulationService: SimulationService
    @SpyK
    lateinit var forgingService: ForgingService
    @SpyK
    lateinit var estimator: FeeEstimator

    /**
     * Given:
     * - A simulation result:
     *  - consumed_gas: 10207
     *  - consumed_storage: 0
     * - A forge result: [FORGE_RESULT]
     * Result should be:
     * - OperationFees(fee=Tez(integerAmount=0, decimalAmount=1409), gasLimit=10307, storageLimit=257)
     */
    @Test
    fun testEstimationOK() {
        callMockDispatcher()
        estimator = initEstimatorWithTransaction()

        val result = estimator.calculateFees(
            OperationType.TRANSACTION,
            fakeTransactionParams,
            fakeAddress,
            fakeMetadata,
            fakeSignature
        ).join()

        Assert.assertNotNull(result)
        Assert.assertEquals(10307, result?.gasLimit)
        Assert.assertEquals(257, result?.storageLimit)
        Assert.assertEquals(Tez("1409"), result?.fee)

        verify(exactly = 1) {
            simulationService.simulate(any(), fakeAddress, fakeMetadata, fakeSignature)
        }

        verify(exactly = 2) {
            forgingService.forge(ForgingPolicy.REMOTE, fakeAddress, any(), fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE, any(), fakeMetadata)
        }
    }

    @Test
    fun testEstimationKO() {
        callErrorDispatcher()
        estimator = initEstimatorWithTransaction()

        estimator.calculateFees(
            OperationType.TRANSACTION,
            fakeTransactionParams,
            fakeAddress,
            fakeMetadata,
            fakeSignature
        ).testFailure(countDownLatch) {
            Assert.assertTrue((it as TezosError).type == TezosErrorType.RPC_ERROR)
        }

        countDownLatch.await()

        verify(exactly = 1) {
            simulationService.simulate(any(), fakeAddress, fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE,fakeAddress, any(), fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE,any(), fakeMetadata)
        }
    }

    @Test
    fun testEstimationWithKOSimulation() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true -> error400()
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true ->
                        validResponse().setBody(RPCConstants.FORGE_RESULT)
                    else -> error404()
                }
            }
        }
        estimator = initEstimatorWithTransaction()
        estimator.calculateFees(
            OperationType.TRANSACTION,
            fakeTransactionParams,
            fakeAddress,
            fakeMetadata,
            fakeSignature
        ).testFailure(countDownLatch) {
            Assert.assertTrue((it as TezosError).type == TezosErrorType.RPC_ERROR)
        }

        countDownLatch.await()

        verify(exactly = 1) {
            simulationService.simulate(any(), fakeAddress, fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE,fakeAddress, any(), fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE,any(), fakeMetadata)
        }
    }

    @Test
    fun testEstimationWithKOForging() {
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(MockDispatcher.getStringFromFile("run_operation_simulation.json"))
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true -> error400()
                    else -> error404()
                }
            }
        }
        estimator = initEstimatorWithTransaction()
        estimator.calculateFees(
            OperationType.TRANSACTION,
            fakeTransactionParams,
            fakeAddress,
            fakeMetadata,
            fakeSignature
        ).testFailure(countDownLatch) {
            Assert.assertTrue((it as TezosError).type == TezosErrorType.RPC_ERROR)
        }

        countDownLatch.await()

        verify(exactly = 1) {
            simulationService.simulate(any(), fakeAddress, fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE,fakeAddress, any(), fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE,any(), fakeMetadata)
        }
    }


    private fun initEstimatorWithTransaction(): FeeEstimator {
        val executor = CurrentThreadExecutor()
        simulationService = spyk(SimulationService(mockClient, executor))
        forgingService = spyk(ForgingService(mockClient, executor))
        return spyk(FeeEstimator(executor, simulationService, forgingService))
    }
}
