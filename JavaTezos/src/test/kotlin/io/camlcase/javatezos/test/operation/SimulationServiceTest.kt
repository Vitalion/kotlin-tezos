/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.operation

import io.camlcase.javatezos.model.operation.OperationMetadata
import io.camlcase.javatezos.operation.SimulationService
import io.camlcase.javatezos.test.network.test.MockServerTest
import io.camlcase.javatezos.test.util.CurrentThreadExecutor
import io.camlcase.javatezos.test.util.Params.Companion.fakeTransactionOperation
import io.camlcase.javatezos.test.util.fakeSignature
import io.camlcase.javatezos.test.util.testFailure
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.CountDownLatch

class SimulationServiceTest : MockServerTest() {
    @Test
    fun testCallOK() {
        callMockDispatcher()
        val service = SimulationService(mockClient, CurrentThreadExecutor())

        val response = service.simulate(
            fakeTransactionOperation,
            "FakeFROMAddress",
            OperationMetadata("", "", "", 1234, null),
            fakeSignature
        ).join()

        Assert.assertNotNull(response)
        Assert.assertEquals(10207, response.consumedGas)
    }

    @Test
    fun testCallKO() {
        callErrorDispatcher()
        val executorService = CurrentThreadExecutor()
        val service = SimulationService(mockClient, executorService)

        val countDownLatch = CountDownLatch(1)
        service.simulate(
            fakeTransactionOperation,
            "FakeFROMAddress",
            OperationMetadata("", "", "", 1234, null),
            fakeSignature
        ).testFailure(countDownLatch)

        countDownLatch.await()
    }
}
