/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.javatezos.test.util

import io.camlcase.javatezos.crypto.PublicKey
import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.data.*
import io.camlcase.javatezos.data.ForgeOperationRPC.Companion.RPC_PATH_FORGE_SUFFIX
import io.camlcase.javatezos.data.ForgeOperationRPC.Companion.RPC_PATH_OPERATION_PREFIX
import io.camlcase.javatezos.data.GetDelegateRPC.Companion.RPC_PATH_DELEGATE_SUFFIX
import io.camlcase.javatezos.data.PreapplyOperationRPC.Companion.RPC_PATH_PREAPPLY_SUFFIX
import io.camlcase.javatezos.model.Tez
import io.camlcase.javatezos.model.operation.OperationMetadata
import io.camlcase.javatezos.model.operation.OperationParams
import io.camlcase.javatezos.model.operation.OperationType
import io.camlcase.javatezos.model.operation.fees.OperationFees
import io.camlcase.javatezos.model.operation.payload.OperationPayload
import io.camlcase.javatezos.model.operation.payload.OperationWithCounter
import io.camlcase.javatezos.operation.OperationFactory

internal class RPCConstants {
    companion object {
        const val RPC_GET_BALANCE = "/chains/main/blocks/head/context/contracts/*/balance"
        const val RPC_GET_MANAGER_KEY = "$RPC_PATH_CONTRACTS*$RPC_PATH_SUFFIX_MANAGER_KEY"
        const val RPC_GET_COUNTER = "$RPC_PATH_CONTRACTS*$RPC_PATH_SUFFIX_COUNTER"
        const val RPC_FORGE_OPERATION = "$RPC_PATH_OPERATION_PREFIX*$RPC_PATH_FORGE_SUFFIX"
        const val RPC_PREAPPLY_OPERATION = "$RPC_PATH_OPERATION_PREFIX*$RPC_PATH_PREAPPLY_SUFFIX"
        const val RPC_GET_DELEGATE = "$RPC_PATH_CONTRACTS*$RPC_PATH_DELEGATE_SUFFIX"
        const val RPC_GET_CONTRACT_STORAGE = "$RPC_PATH_CONTRACTS*$RPC_PATH_CONTRACT_STORAGE_SUFFIX"
        const val RPC_GET_BIG_MAP_VALUE = "$RPC_PATH_CONTRACTS*$RPC_PATH_BIG_MAP_SUFFIX"

        const val FORGE_RESULT =
            "8a700ab69b834bc5aa3b892f809bf2dc2cfdb61700fd8cd3197884954aec76e56c00b9232920ab58cabc9fa969b5cae8a87bf8827fe79d0a80d80880ea30e0d4030100007659a8eb542f8e3cfb24259dc6650dec5e9a1a3300"
        const val INJECT_RESULT = "ooB7xxBrYmZMVFfotEYFJuexVGNrFEZT9Z8FvweExj6RcYoM1x6"
        const val STORAGE_RESULT = "{\"string\":\"KT194hQVCPCsCYUhDjbeUCwpKcKyzN3AZLbc\"}"
        const val BIG_MAP_RESULT = "{\"prim\":\"Pair\",\"args\":[{\"int\":\"1000000\"}]}\n"
    }
}

internal class BabylonAddress {
    companion object {
        const val TEST_ADDRESS_TZ1_CW = "tz1cWwpcWBo8LcZiyfPdYYe7xifu1zGL4agT"
        const val TEST_ADDRESS_TZ1_WRO = "tz1WRoqRuEZqeh2MXpxEeKmGk89Wv9Ad4GyC"
    }
}


internal val fakeSignature = object : SignatureProvider {
    override val publicKey: PublicKey
        get() = PublicKey(ByteArray(8))

    override fun sign(hex: String): ByteArray {
        return ByteArray(8)
    }
}

internal class Params {
    companion object {
        const val fakeAddress = "tz1ADDRESS"
        val fakeFees = OperationFees(Tez(1.0), 200, 300)
        val fakeMetadata =
            OperationMetadata("FakeBranch", "FakeProtocol", "FakeChainId", 1234, null)
        val fakeTransactionParams = OperationParams.Transaction(Tez(1.0), "tz1FROM", "tz1TO")
        val fakeTransactionOperation =
            OperationFactory.createOperation(
                OperationType.TRANSACTION,
                fakeTransactionParams,
                fakeFees
            )!!
        val fakeOperationWithCounter = OperationWithCounter(fakeTransactionOperation, 1234)
        val fakeOperationPayload = OperationPayload(listOf(fakeOperationWithCounter), "FakeBranch")
    }
}
