/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos

import io.camlcase.javatezos.crypto.PublicKey
import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.crypto.SigningService.sign
import io.camlcase.javatezos.data.*
import io.camlcase.javatezos.ext.badArgumentsError
import io.camlcase.javatezos.ext.wrap
import io.camlcase.javatezos.michelson.MichelsonComparable
import io.camlcase.javatezos.michelson.MichelsonParameter
import io.camlcase.javatezos.model.*
import io.camlcase.javatezos.model.operation.*
import io.camlcase.javatezos.model.operation.payload.OperationPayload
import io.camlcase.javatezos.network.DefaultNetworkClient
import io.camlcase.javatezos.operation.*
import io.camlcase.javatezos.operation.fees.OperationFeesFactory
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.onComplete
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * Main entrypoint and provider of operations to do on the Tezos Network.
 *
 * @param networkClient implementation for connection. By default, a [DefaultNetworkClient]
 * @param executorService to manage network requests and calculations asynchronously. By default, a [ThreadPoolExecutor] of 3
 */
class TezosNodeClient(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) {
    /**
     * Retrieves metadata for operations
     */
    private val metadataService: OperationMetadataService

    /**
     * Simulates operations to estimate fees
     */
    private val simulationService: SimulationService

    /**
     * A service which forges operations.
     */
    private val forgingService: ForgingService

    /**
     * A service that preapplies operations.
     */
    private val preapplyService: PreapplyService

    private val operationFeesFactory: OperationFeesFactory

    /**
     * Initialize client with default configuration
     */
    constructor() : this(MAINNET_TEZTECH_NODE)

    /**
     * Initialize client with default configuration
     * @param nodeUrl The path to the remote node
     */
    constructor(nodeUrl: String) : this(DefaultNetworkClient(nodeUrl), Executors.newFixedThreadPool(3))

    init {
        metadataService = OperationMetadataService(networkClient, executorService)
        simulationService = SimulationService(networkClient, executorService)
        forgingService = ForgingService(networkClient, executorService)
        preapplyService = PreapplyService(networkClient)
        operationFeesFactory = OperationFeesFactory(executorService, simulationService, forgingService)
    }

    /**
     * Call this when finishing to avoid leaks.
     */
    fun clear() {
        executorService.shutdown()
    }

    fun getHead(callback: TezosCallback<Map<String, Any>>) {
        networkClient.send(GetChainHeadRPC(), callback)
    }

    /**
     * Retrieve the hash of the block at the head of the chain.
     */
    fun getHeadHash(callback: TezosCallback<String>) {
        networkClient.send(GetChainHeadHashRPC(), callback)
    }

    /**
     * Retrieve the address manager key for the given address.
     */
    fun getManagerKey(address: Address, callback: TezosCallback<String>) {
        networkClient.send(GetAddressManagerKeyRPC(address), callback)
    }

    /**
     * Retrieve the balance of [Tez] of a given [Address]
     */
    fun getBalance(address: Address, callback: TezosCallback<Tez>) {
        networkClient.send(GetBalanceRPC(address), callback)
    }

    /**
     * Transact Tezos between accounts.
     *
     * @param to The address which will receive the Tez.
     * @param from The address sending the balance.
     * @param signatureProvider which will sign the operation
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun send(
        amount: Tez,
        to: Address,
        from: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.TRANSACTION,
            from,
            OperationParams.Transaction(amount, from, to),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Originate a new account from the given account.
     *
     * @param managerAddress The address which will manage the new account.
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun originateAccount(
        managerAddress: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.ORIGINATION,
            managerAddress,
            OperationParams.Origination(managerAddress),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Register an address as a delegate.
     *
     * @param delegate The address registering as a delegate.
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun registerDelegate(
        delegate: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.DELEGATION,
            delegate,
            OperationParams.Delegation(delegate, delegate),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Delegate the balance of an originated account.
     *
     * @param source The address which will delegate.
     * @param delegate The address which will receive the delegation.
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [source]'s tz1 Address.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun delegate(
        source: Address,
        delegate: Address,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.DELEGATION,
            source,
            OperationParams.Delegation(source, delegate),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Divulge [publicKey] to the block chain
     *
     * @param source The address which will run the reveal.
     * @param publicKey The key to divulge
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [publicKey].
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun reveal(
        source: Address,
        publicKey: PublicKey,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.REVEAL,
            source,
            OperationParams.Reveal(source, publicKey),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Call a smart contract.
     *
     * @param source The address invoking the contract.
     * @param contract The smart contract to invoke.
     * @param amount The amount of Tez to transfer with the invocation. Default is 0.
     * @param parameter An optional parameter to send to the smart contract. Default is none.
     * @param signatureProvider The object which will sign the operation. Must be the one used to manage the keys of [source].
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun call(
        source: Address,
        contract: Address,
        amount: Tez = Tez.zero,
        parameter: MichelsonParameter? = null,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        runOperation(
            OperationType.TRANSACTION,
            source,
            OperationParams.Transaction(amount, source, contract, parameter),
            signatureProvider,
            feesPolicy,
            callback
        )
    }

    /**
     * Runs a given type of operation.
     *
     * @param signatureProvider The object which will sign the operation.
     * @param feesPolicy A policy to apply when determining operation fees. Default is default fees.
     * @param callback With a string representing the transaction ID hash if the operation was successful.
     */
    fun runOperation(
        type: OperationType,
        source: Address,
        params: OperationParams,
        signatureProvider: SignatureProvider,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Default(),
        callback: TezosCallback<String>
    ) {
        var metadata: OperationMetadata? = null
        // Get metadata from the source
        metadataService.getMetadata(source)
            // Estimate fees
            .flatMap(executorService) {
                metadata = it
                operationFeesFactory
                    .calculateFees(type, params, source, feesPolicy, it, signatureProvider)
            }
            // Create Operation with calculated fees
            .map(executorService) {
                OperationFactory.createOperation(type, params, it)
            }
            // Forge
            .flatMap(executorService) {
                forge(it, source, metadata, signatureProvider)
            }
            // Sign
            .map(executorService) {
                sign(it.operationPayload, metadata!!, it.forgeResult, signatureProvider)
            }
            // Preapply
            .flatMap(executorService) {
                preapply(it, metadata)
            }
            // Inject
            .flatMap(executorService) {
                inject(it)
            }
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Forge an operation
     *
     * @throws TezosError if [operation] or [metadata] are null
     */
    private fun forge(
        operation: Operation?,
        address: Address,
        metadata: OperationMetadata?,
        signatureProvider: SignatureProvider
    ): CompletableFuture<ForgeResponse> {
        if (operation == null || metadata == null) {
            throw badArgumentsError("Could not retrieve operation or metadata")
        }
        val payload = OperationPayload(listOf(operation).asIterable(), address, metadata, signatureProvider)
        return forgingService.forge(ForgingPolicy.REMOTE, payload, metadata)
            .map { ForgeResponse(payload, it) }
    }

    /**
     * Preapply an operation
     *
     * @return Completable with signedBytes for injection
     * @throws TezosError if preapplication returns a failed state.
     */
    private fun preapply(response: SigningResponse, metadata: OperationMetadata?): CompletableFuture<String> {
        return preapplyService.preapply(response.signedProtocolOperationPayload, metadata!!)
            .map(executorService) {
                if (it.isNullOrEmpty()) {
                    response.signedBytes
                } else {
                    throw TezosError(TezosErrorType.PREAPPLICATION_ERROR, rpcErrors = it)
                }
            }
    }

    /**
     * Inject the given hex into the remote node.
     * @param signedBytes A hex payload to inject.
     * @return Completable with the operation hash
     */
    private fun inject(signedBytes: String): CompletableFuture<String?> {
        return networkClient.send(InjectOperationRPC(signedBytes))
    }

    /**
     * Retrieve the delegate of a given [Address]
     */
    fun getDelegate(address: Address, callback: TezosCallback<String>) {
        networkClient.send(GetDelegateRPC(address), callback)
    }

    /**
     * Retrieve the storage of a smart contract.
     * @param address The address of the smart contract to inspect.
     * @param callback Completion callback which will be called with the storage.
     */
    fun getContractStorage(address: Address, callback: TezosCallback<Map<String, Any>>) {
        networkClient.send(GetContractStorageRPC(address), callback)
    }

    /**
     * Inspect the value of a big map in a smart contract.
     * @param address The address of a smart contract with a big map.
     * @param key The key in the big map to look up.
     */
    fun getBigMapValue(
        address: Address,
        key: MichelsonParameter,
        type: MichelsonComparable,
        callback: TezosCallback<Map<String, Any>>
    ) {
        networkClient.send(GetBigMapValueRPC(address, key, type), callback)
    }

    companion object {
        // MainNet
        const val MAINNET_TEZTECH_NODE = "https://rpc.tezrpc.me"
        const val MAINNET_CRYPTIUM_NODE = "https://mainnet.tezos.cryptium.ch:8732"
        // BabylonNet
        const val BABYLONNET_TEZTECH_NODE = "https://rpcalpha.tzbeta.net"
        const val BABYLONNET_CRYPTIUM_NODE = "http://babylonnet.tezos.cryptium.ch:8732"
        // CarthageNet
        const val CARTHAGENET_CRYPTIUM_NODE = "http://carthagenet.tezos.cryptium.ch:8732"
    }
}
