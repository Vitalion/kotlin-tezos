/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.ext

import io.camlcase.javatezos.data.Parser
import io.camlcase.javatezos.data.parser.RPCErrorParser
import io.camlcase.javatezos.model.RPCErrorResponse
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener

/**
 * Given a "failed" status in a results JSON, extract the "errors" into a list.
 */
fun <T> Parser<T>?.parseErrors(results: Map<*, *>): List<RPCErrorResponse> {
    this?.let {
        val errors = results["errors"] as List<*>
        val parser = RPCErrorParser()
        val list = ArrayList<RPCErrorResponse>()
        for (error in errors) {
            val map = error as Map<String, Any>
            parser.parse(map.toJson().toString())?.also {
                list.add(it)
            }
        }
        return list
    } ?: return emptyList()
}

/**
 * Given an array of mapable errors, extracts a readable list
 */
fun <T> Parser<T>?.parseErrors(array: JSONArray): List<RPCErrorResponse> {
    this?.let {
        val parser = RPCErrorParser()
        val list = ArrayList<RPCErrorResponse>()
        var i = 0
        while (i < array.length()) {
            val map = array[i] as JSONObject
            parser.parse(map.toString())?.also {
                list.add(it)
            }
            i++
        }
        return list
    } ?: return emptyList()
}

/**
 * Given a json with errors such as:
 *    [
 *      {"kind":"branch","id":"proto.current_protocol.error1.description","contract":"tz1(...))"},
 *      {"kind":"branch","id":"proto.current_protocol.error2.description","contract":"tz1(...))"}
 *    ]
 * Parses the given [jsonData] and returns a list of readable [RPCErrorResponse]
 * @throws JSONException if [jsonData] does not have JSON syntax.
 */
fun <T> Parser<T>?.parseGeneralErrors(jsonData: String): List<RPCErrorResponse>? {
    return this?.let {
        val json = JSONTokener(jsonData).nextValue()
        if (json is JSONArray) {
            return parseErrors(JSONArray(jsonData))
        }
        return null
    }
}
