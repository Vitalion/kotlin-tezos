/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.operation

import io.camlcase.javatezos.model.operation.*
import io.camlcase.javatezos.model.operation.fees.OperationFees

/**
 * A factory which produces operations
 */
internal object OperationFactory {

    /**
     * Given a [type], returns an [Operation]
     *
     * @return null if [params] are not the correct instantiation for that operation
     */
    fun createOperation(
        type: OperationType,
        params: OperationParams,
        fees: OperationFees
    ): Operation? {
        return when (type) {
            OperationType.TRANSACTION -> {
                (params as? OperationParams.Transaction)?.let {
                    return createTransactionOperation(it, fees)
                }
            }
            OperationType.DELEGATION ->
                (params as? OperationParams.Delegation)?.let {
                    return createDelegationOperation(it, fees)
                }
            OperationType.REVEAL -> {
                (params as? OperationParams.Reveal)?.let {
                    return createRevealOperation(it, fees)
                }
            }
            OperationType.ORIGINATION -> {
                (params as? OperationParams.Origination)?.let {
                    return createOriginationOperation(it, fees)
                }
            }
        }
    }

    private fun createTransactionOperation(
        params: OperationParams.Transaction,
        fees: OperationFees
    ): TransactionOperation? {
        return TransactionOperation(
            params.amount,
            params.from,
            params.to,
            fees,
            params.parameter
        )
    }

    private fun createOriginationOperation(
        params: OperationParams.Origination,
        fees: OperationFees
    ): OriginationOperation {
        return OriginationOperation(params.source, fees)
    }

    private fun createDelegationOperation(
        params: OperationParams.Delegation,
        fees: OperationFees
    ): DelegationOperation {
        return DelegationOperation(params.source, params.delegate, fees)
    }

    private fun createRevealOperation(params: OperationParams.Reveal, fees: OperationFees): RevealOperation {
        return RevealOperation(params.source, params.publicKey, fees)
    }
}
