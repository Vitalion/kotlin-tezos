/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.operation

import io.camlcase.javatezos.NetworkClient
import io.camlcase.javatezos.data.GetAddressCounterRPC
import io.camlcase.javatezos.data.GetAddressManagerKeyRPC
import io.camlcase.javatezos.data.GetChainHeadRPC
import io.camlcase.javatezos.ext.wrap
import io.camlcase.javatezos.model.Address
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.model.operation.OperationMetadata
import io.github.vjames19.futures.jdk8.map
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.function.Function

/**
 * Helper class to centralize metadata retrieval before operations
 */
class OperationMetadataService(
    private val networkClient: NetworkClient,
    private val executorService: ExecutorService
) {
    /**
     * Retrieve metadata needed to forge / pre-apply / sign / inject an operation.
     *
     * @throws [TezosError] through the Completable stream if something goes wrong
     */
    fun getMetadata(address: Address): CompletableFuture<OperationMetadata> {
        val headHashCompletable = getHeadHash()
        val managerKeyCompletable = getManagerKey(address)
        val counterCompletable = getCounter(address)
        return CompletableFuture.allOf(headHashCompletable, counterCompletable, managerKeyCompletable)
            .thenApplyAsync(Function {
                val (hash, protocol, chainId) = headHashCompletable.join()
                val counter = counterCompletable.join()
                val key = managerKeyCompletable.join()
                if (hash == null || protocol == null || chainId == null || counter == null) {
                    throw IllegalStateException("[OperationMetadata] Invalid response for: {hash : $hash},\n{protocol: $protocol},\n{chain_id: $chainId},\n{counter: $counter}").wrap()
                }
                OperationMetadata(hash, protocol, chainId, counter, key)
            }, executorService)
    }

    /**
     * Retrieve chain info counter for the given address.
     * @return CompletableFuture of Pair of nullable (hash, protocol).
     */
    private fun getHeadHash(): CompletableFuture<Triple<String?, String?, String?>> {
        return networkClient.send(GetChainHeadRPC())
            .map {
                val hash = it?.get("hash") as? String
                val chainId = it?.get("chain_id") as? String
                val protocol = it?.get("protocol") as? String
                Triple(hash, protocol, chainId)
            }
    }

    private fun getCounter(address: Address): CompletableFuture<Int?> {
        return networkClient.send(GetAddressCounterRPC(address))
    }

    /**
     * Retrieve the base58check encoded public key for the given address.
     */
    private fun getManagerKey(address: Address): CompletableFuture<String?> {
        return networkClient.send(GetAddressManagerKeyRPC(address))
    }
}
