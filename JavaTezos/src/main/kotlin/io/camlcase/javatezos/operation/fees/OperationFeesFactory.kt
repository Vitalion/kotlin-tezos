/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.operation.fees

import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.model.Address
import io.camlcase.javatezos.model.operation.OperationMetadata
import io.camlcase.javatezos.model.operation.OperationParams
import io.camlcase.javatezos.model.operation.OperationType
import io.camlcase.javatezos.model.operation.fees.OperationFees
import io.camlcase.javatezos.operation.ForgingService
import io.camlcase.javatezos.operation.OperationFeesPolicy
import io.camlcase.javatezos.operation.SimulationService
import io.github.vjames19.futures.jdk8.ImmediateFuture
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

internal class OperationFeesFactory(
    private val executorService: ExecutorService,
    private val simulationService: SimulationService,
    private val forgingService: ForgingService
) {

    fun calculateFees(
        type: OperationType,
        params: OperationParams,
        from: Address,
        operationFeePolicy: OperationFeesPolicy,
        metadata: OperationMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<OperationFees> {
        return when (operationFeePolicy) {
            is OperationFeesPolicy.Default -> ImmediateFuture {
                DefaultFeeEstimator.calculateFees(metadata.tezosProtocol, type)
            }
            is OperationFeesPolicy.Custom -> ImmediateFuture { operationFeePolicy.fees }
            is OperationFeesPolicy.Estimate -> {
                FeeEstimator(executorService, simulationService, forgingService)
                    .calculateFees(type, params, from, metadata, signatureProvider)
            }
        }
    }

}
