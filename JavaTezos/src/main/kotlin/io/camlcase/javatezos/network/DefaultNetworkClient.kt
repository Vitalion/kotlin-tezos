/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.network

import io.camlcase.javatezos.ClientWrapper
import io.camlcase.javatezos.NetworkClient
import io.camlcase.javatezos.data.Header
import io.camlcase.javatezos.data.Parser
import io.camlcase.javatezos.data.RPC
import io.camlcase.javatezos.data.parser.RPCErrorParser
import io.camlcase.javatezos.ext.parseGeneralErrors
import io.camlcase.javatezos.ext.wrap
import io.camlcase.javatezos.model.TezosCallback
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.model.TezosErrorType
import okhttp3.*
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.TimeUnit

/**
 * Standard implementation of [NetworkClient] using a [DefaultClientWrapper].
 *
 * Threading will be delegated to the wrapper's client Executor.
 */
class DefaultNetworkClient(
    private val baseUrl: String,
    private val wrapper: DefaultClientWrapper
) : NetworkClient {
    constructor(baseUrl: String) : this(baseUrl, DefaultClientWrapper())

    override fun <T> send(rpc: RPC<T>, callback: TezosCallback<T>) {
        send(
            rpc,
            { callback.onSuccess(it) },
            { callback.onFailure(it.wrap(TezosErrorType.RPC_ERROR)) })
    }

    override fun <T> send(rpc: RPC<T>): CompletableFuture<T?> {
        val future = CompletableFuture<T?>()
        send(
            rpc,
            { future.complete(it) },
            { future.completeExceptionally(it.wrap(TezosErrorType.RPC_ERROR)) })
        return future
    }

    private fun <T> send(rpc: RPC<T>, onSuccess: (T?) -> Unit, onError: (Throwable) -> Unit) {
        val request = prepareRequest(rpc)
        try {
            wrapper.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    onError(e)
                }

                override fun onResponse(call: Call, response: Response) {
                    parseResponse(response, rpc.parser, onSuccess, onError)
                }
            })
        } catch (e: Exception) {
            onError(e)
        }
    }

    private fun prepareRequest(rpc: RPC<*>): Request {
        val request = Request.Builder()
            .url(baseUrl + rpc.endpoint)

        rpc.header.forEach {
            request.addHeader(it.field, it.value)
        }

        rpc.payload?.apply {
            // Use RequestBody constructor that doesn't add "; charset=utf-8"
            val body = toByteArray().toRequestBody(Header.JSON_MEDIATYPE)
            request.post(body)
        }

        return request.build()
    }

    private fun <T> parseResponse(
        response: Response,
        parser: Parser<T>,
        onSuccess: (T?) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        // Only get the body once
        val body = response.body?.string()
        if (response.code == 200) {
            try {
                val parsedResponse = parseResponseBody(body, parser)
                onSuccess(parsedResponse)
            } catch (e: Throwable) {
                onError(e)
            }
        } else {
            onError(parseError(response, body))
        }
    }

    private fun <T> parseResponseBody(body: String?, parser: Parser<T>): T? {
        return if (body == null || body.startsWith("null")) {
            null
        } else {
            parser.parse(body)
        }
    }

    private fun parseError(response: Response, errorBody: String?): TezosError {
        val rpcErrors = if (errorBody.isNullOrBlank()) {
            emptyList()
        } else {
            RPCErrorParser().parseGeneralErrors(errorBody)
        }
        return TezosError(
            TezosErrorType.RPC_ERROR,
            rpcErrors,
            IOException("${response.code}: ${response.message}")
        )
    }
}

/**
 * Provides an OKHttp client implementation to be used.
 *
 * @param executorService to set a custom Dispatcher used to handle asynchronous requests. By default null, so we use OkHttp's default.
 */
class DefaultClientWrapper(
    private val executorService: ExecutorService? = null
) : ClientWrapper<OkHttpClient> {
    /**
     * Value of client is lazy loaded to only provide one instance.
     *
     * OkHttp performs best when you create a single OkHttpClient instance and reuse it for all of your HTTP calls. This
     * is because each client holds its own connection pool and thread pools. Reusing connections and threads reduces
     * latency and saves memory. Conversely, creating a client for each request wastes resources on idle pools.
     *
     * @see http://square.github.io/okhttp/3.x/okhttp/okhttp3/OkHttpClient.html
     */
    override val client: OkHttpClient by lazy {
        val builder = OkHttpClient().newBuilder()
            .connectTimeout(1000, TimeUnit.MILLISECONDS)
            .writeTimeout(1000, TimeUnit.MILLISECONDS)
            .readTimeout(1000, TimeUnit.MILLISECONDS)

        executorService?.let {
            builder.dispatcher(Dispatcher(it))
        }

        builder.build()
    }
}
