/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos

import io.camlcase.javatezos.data.RPC
import io.camlcase.javatezos.model.TezosCallback
import java.util.concurrent.CompletableFuture

/**
 * Protocol for implementing requests
 */
interface NetworkClient {
    /**
     * Send an RPC
     *
     * @param callback Custom callback for
     */
    fun <T> send(rpc: RPC<T>, callback: TezosCallback<T>)
    /**
     * Send an RPC
     *
     * @return a [CompletableFuture] that can be composed with other calls.
     */
    fun <T> send(rpc: RPC<T>): CompletableFuture<T?>
}

/**
 * Wrapper for a specific type of client to be used in the [NetworkClient]
 */
interface ClientWrapper<C> {
    val client: C
}


