/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.data

import io.camlcase.javatezos.data.ForgeOperationRPC.Companion.RPC_PATH_OPERATION_PREFIX
import io.camlcase.javatezos.data.parser.PreapplyResponseParser
import io.camlcase.javatezos.ext.toJsonArray
import io.camlcase.javatezos.model.RPCErrorResponse
import io.camlcase.javatezos.model.operation.payload.SignedProtocolOperationPayload

/**
 * An RPC which will pre-apply an operation.
 *
 * @param signedProtocolOperationPayload A payload to send with the operation.
 */
class PreapplyOperationRPC(
    branch: String,
    signedProtocolOperationPayload: SignedProtocolOperationPayload
) : RPC<List<RPCErrorResponse>>(
    endpoint = RPC_PATH_OPERATION_PREFIX + branch + RPC_PATH_PREAPPLY_SUFFIX,
    header = listOf(Header.plainTextHeader),
    payload = signedProtocolOperationPayload.payload.toJsonArray(),
    parser = PreapplyResponseParser()
) {
    companion object {
        internal const val RPC_PATH_PREAPPLY_SUFFIX = "/helpers/preapply/operations"
    }
}
