/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.data.parser

import io.camlcase.javatezos.data.Parser
import io.camlcase.javatezos.ext.parseErrors
import io.camlcase.javatezos.model.RPCErrorResponse
import org.json.JSONArray
import org.json.JSONObject

class PreapplyResponseParser : Parser<List<RPCErrorResponse>> {
    override fun parse(jsonData: String): List<RPCErrorResponse>? {
        val jsonArray = JSONArray(jsonData)
        val list: ArrayList<RPCErrorResponse> = ArrayList()
        for (map in jsonArray) {
            val parsedMap = StringMapParser().parse(map.toString())
            parsedMap?.let { map ->
                val contents = map["contents"] as JSONArray
                for (content in contents) {
                    val contentMap = (content as JSONObject).toMap()
                    val metadata = contentMap["metadata"] as Map<*, *>
                    val results = metadata["operation_result"] as Map<*, *>
                    if (results["status"] == "failed") {
                        list.addAll(parseErrors(results))
                    }
                }
            }
        }
        return list
    }

}
