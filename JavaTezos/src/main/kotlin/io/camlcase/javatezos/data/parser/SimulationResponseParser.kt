/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.data.parser

import io.camlcase.javatezos.data.Parser
import io.camlcase.javatezos.ext.parseErrors
import io.camlcase.javatezos.ext.parseGeneralErrors
import io.camlcase.javatezos.model.TezosError
import io.camlcase.javatezos.model.TezosErrorType
import io.camlcase.javatezos.model.operation.SimulationResponse
import org.json.JSONArray
import org.json.JSONObject

/**
 * Parse the resulting JSON from a simulation operation to a [SimulationResponse].
 */
internal class SimulationResponseParser : Parser<SimulationResponse> {
    override fun parse(jsonData: String): SimulationResponse? {
        val errors = parseGeneralErrors(jsonData)
        if (!errors.isNullOrEmpty()) {
            throw TezosError(TezosErrorType.RPC_ERROR, errors)
        }

        val parsedMap = StringMapParser().parse(jsonData)
        parsedMap?.let { map ->
            try {
                var consumedGas = 0
                var consumedStorage = 0

                val contents = map["contents"] as JSONArray
                for (content in contents) {
                    val contentMap = (content as JSONObject).toMap()

                    val metadata = contentMap["metadata"] as Map<*, *>
                    val results = metadata["operation_result"] as Map<*, *>
                    if (results["status"] == "failed") {
                        throw TezosError(TezosErrorType.RPC_ERROR, parseErrors(results))

                    }
                    consumedGas += (results["consumed_gas"] as? String)?.toInt() ?: 0
                    consumedStorage += (results["storage_size"] as? String)?.toInt() ?: 0

                    val internalResults = metadata["internal_operation_results"] as? List<*>
                    internalResults?.let {
                        for (internalResult in it) {
                            val (internalConsumedGas, internalConsumedStorage) = parseInternalOperationResult(
                                internalResult
                            )

                            consumedGas += internalConsumedGas
                            consumedStorage += internalConsumedStorage
                        }
                    }
                }

                return SimulationResponse(consumedGas, consumedStorage)
            } catch (e: TezosError) {
                throw e
            } catch (e: Exception) {
                return null
            }
        } ?: return null
    }

    private fun parseInternalOperationResult(internalResult: Any?): Pair<Int, Int> {
        val result = (internalResult as Map<*, *>)["result"] as Map<*, *>
        val internalConsumedGas = (result["consumed_gas"] as? String)?.toInt() ?: 0
        val internalConsumedStorage = (result["storage_size"] as? String)?.toInt() ?: 0
        return Pair(internalConsumedGas, internalConsumedStorage)
    }
}
