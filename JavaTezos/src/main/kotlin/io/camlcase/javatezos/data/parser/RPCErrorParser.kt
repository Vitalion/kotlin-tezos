/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.data.parser

import io.camlcase.javatezos.data.Parser
import io.camlcase.javatezos.model.RPCErrorResponse
import io.camlcase.javatezos.model.RPCErrorType

/**
 * Parse a JSON String to a readable [RPCErrorResponse]
 * @see [TezosError]
 */
internal class RPCErrorParser : Parser<RPCErrorResponse> {
    override fun parse(jsonData: String): RPCErrorResponse? {
        val parsedMap = StringMapParser().parse(jsonData)
        parsedMap?.let {
            return try {
                val kind = (parsedMap["kind"] as String).toUpperCase()
                val message = (parsedMap["msg"] as String?)?.replace("\n", " ")
                val cause = parsedMap["id"] as String
                val contract = parsedMap["contract"] as String?
                RPCErrorResponse(RPCErrorType.valueOf(kind), message ?: cause, contract)
            } catch (e: Exception) {
                null
            }
        } ?: return null
    }
}
