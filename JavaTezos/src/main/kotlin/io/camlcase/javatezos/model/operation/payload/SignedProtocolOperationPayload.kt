/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.model.operation.payload

import io.camlcase.javatezos.data.Payload

/**
 * An operation payload that is signed and bound to a specific protocol.
 *
 * @param signedOperationPayload An operation payload and associated signature.
 * @param protocol The hash of the protocol for the payload.
 */
data class SignedProtocolOperationPayload(
    val signedOperationPayload: SignedOperationPayload,
    val protocol: String
) : Payload {

    /**
     * Implemented specifically for preapply operation:
     *
     *   { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"
     *   "branch": $block_hash,
     *   "contents": [ $operation.alpha.contents ... ],
     *   "signature": $Signature }
     */
    override val payload: MutableMap<String, Any>
        get() {
            val operationPayload = signedOperationPayload.operationPayload.payload
            operationPayload[SignedOperationPayload.PAYLOAD_ARG_SIGNATURE] = signedOperationPayload.signature
            operationPayload[PAYLOAD_ARG_PROTOCOL] = protocol
            return operationPayload
        }

    companion object {
        const val PAYLOAD_ARG_PROTOCOL = "protocol"
    }

}
