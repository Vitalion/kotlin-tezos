/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.model

import io.camlcase.javatezos.crypto.PublicKey
import io.camlcase.javatezos.crypto.SecretKey
import io.camlcase.javatezos.crypto.SignatureProvider
import io.camlcase.javatezos.crypto.mnemonic.MnemonicFacade
import io.camlcase.javatezos.model.Wallet.Companion.invoke

/**
 * A model of a wallet in the Tezos ecosystem.
 *
 * Clients can create a new wallet by calling the constructor operators [invoke]. Clients can also restore an existing wallet by providing an [mnemonic] and optional passphrase.
 *
 * @param publicKey [PublicKey] encapsulation for the Wallet
 * @param secretKey [SecretKey] encapsulation for the Wallet
 * @param address A base58check encoded public key hash for the wallet, prefixed with "tz1" which represents an address in the Tezos ecosystem.
 * @param mnemonic If this wallet was generated from a mnemonic, a list of english mnemonic words used to generate the wallet with the BIP39 specification, otherwise nil.
 */
data class Wallet(
    override val publicKey: PublicKey,
    val secretKey: SecretKey,
    val address: Address,
    val mnemonic: List<String>? = null
) : SignatureProvider {

    /**
     * Sign the given hexadecimal encoded string with the wallet's [secretKey]
     * @param hex The string to sign.
     * @return A signature from the input.
     */
    override fun sign(hex: String): ByteArray? {
        return secretKey.sign(hex)
    }

    companion object {

        /**
         * Constructor: Create a new wallet with the given mnemonic and encrypted with an optional passphrase.
         * If no mnemonic given, it will try to generate one.
         *
         * @return The Wallet or null if error while generating mnemonic or keys.
         */
        operator fun invoke(mnemonic: List<String>? = null, passphrase: String = "") = run {
            val validatedMnemonic =  if (mnemonic.isNullOrEmpty()) {
                MnemonicFacade.generateMnemonic(128)
            } else {
                mnemonic
            }

            validatedMnemonic?.let { listOfWords ->
                MnemonicFacade.seedString(listOfWords, passphrase)?.let {
                    val secretKey = SecretKey(seedString = it)
                    val publicKey = PublicKey(secretKey)
                    val address = publicKey?.hash
                    if (secretKey == null || publicKey == null || address == null) {
                        return null
                    }
                    Wallet(publicKey, secretKey, address, listOfWords)
                }
            }
        }
    }
}
