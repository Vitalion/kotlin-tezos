/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.javatezos.model


/**
 * Representation of NanoTez
 * (1 tez = 106 mutez = 109 nanotez)
 */
typealias NanoTez = Int

/**
 * The number of nanotez in a single mutez.
 */
internal const val KNANOTEZ_PER_MUTEZ = 1_000

/**
 * Convert the given amount of NanoTez to a Tez object.
 */
internal fun NanoTez.toTez(): Tez {
    val mutez = if (this % KNANOTEZ_PER_MUTEZ == 0) {
        this / KNANOTEZ_PER_MUTEZ
    } else {
        (this / KNANOTEZ_PER_MUTEZ) + 1
    }
    return Tez(mutez.toString())
}
