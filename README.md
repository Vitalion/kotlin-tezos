# KotlinTezos

[![Relase Version](https://img.shields.io/maven-central/v/io.camlcase/javatezos.svg?label=release)](http://search.maven.org/#search%7Cga%7C1%7Cjavatezos)


KotlinTezos is an Android/Kotlin SDK for operating with the [Tezos Blockchain](https://tezos.gitlab.io/tezos/index.html).

The project can be considered a fork of [TezosKit](https://github.com/keefertaylor/TezosKit) as design, purpose and
 documentation are identical.


## Getting started

### Installation

The minimum API level for this library is [24](https://developer.android.com/about/versions/nougat/android-7.0.html).

```
implementation 'io.camlcase:javatezos:LATEST_VERSION'
```

### TezosNodeClient

The main entrypoint for operating with the node is the `TezosNodeClient` class

##### Kotlin

```
val client = TezosNodeClient("NODE_URL")
client.getBalance("ADDRESS", object : TezosCallback<Tez> {
    override fun onSuccess(item: Tez?) {
        println("*** CALLBACK onSuccess  $item")
    }

    override fun onFailure(error: TezosError) {
        println("*** CALLBACK onFailure $error")
    }
})
```
##### Java
```
TezosCallback<String> callback = new TezosCallback<Tez>() {
    @Override
    public void onSuccess(@Nullable Tez item) {
        System.out.println("*** CALLBACK onSuccess " + item);
    }

    @Override
    public void onFailure(@NotNull TezosError error) {
        System.out.println("*** CALLBACK onFailure " + error);
    }
};
TezosNodeClient client = new TezosNodeClient("NODE_URL");
client.getBalance("ADDRESS", callback);
```

### Wallet

Accounts in Tezos are represented with `Wallet` objects. The library provides mnemonic and key generation.

##### Kotlin

```
val listOfWords = listOf("A", "B", "C")
val wallet = Wallet(listOfWords)
```

##### Java
```
List<String> mnemonic = new ArrayList<>(Arrays.asList("A", "B", "C"));
Wallet wallet = Wallet.Companion.invoke(mnemonic, "PASSPHRASE");

```

### Callbacks and Completables

The `TezosNodeClient` returns the result via an asynchronous `TezosCallback`. The library uses [`CompletableFuture`](https://developer.android.com/reference/kotlin/java/util/concurrent/CompletableFuture?hl=en) internally to combine threads and it has a [`ExecutorService`](https://developer.android.com/reference/java/util/concurrent/ExecutorService) as an argument of the client for finetuning that control.

## Dependencies

Name | Version | License
--- | --- | ---
**OkHttp** | `4.3.1` | [Apache License 2.0](https://github.com/square/okhttp/blob/master/LICENSE.txt)
**JSON** | `20190722` | [JSON License](https://www.json.org/license.html)
**Kotlin Futures JDK8** | `1.2.0` | [MIT License](https://github.com/vjames19/kotlin-futures/blob/master/LICENSE)
**LazySodium-Android** | `4.2.3` | [Mozilla Public License 2.0](https://github.com/terl/lazysodium-java/blob/master/LICENSE.md)
**Java Native Access** | `5.5.0` | [Apache License 2.0](https://github.com/java-native-access/jna/blob/master/LICENSE)
**Fat AAR Android** | `1.2.8` | [Apache License 2.0](https://github.com/kezong/fat-aar-android/blob/master/LICENSE)


## Attribution

This project uses source code from these libraries:

Name |  License
--- | --- 
BitcoinJ |  [Apache License 2.0](https://github.com/bitcoinj/bitcoinj/blob/master/COPYING)


## Contribute

Contributions and bug fixes are appreciated. If you find any bugs or vulnerabilities, please feel free to open an issue or contact us at: malena@camlcase.io

Our main objective in the future is to convert this project into a multiplatform JVM/ARM one. If you feel you can help with this task, don't hesitate to open a merge request or contact us with your suggestion.


